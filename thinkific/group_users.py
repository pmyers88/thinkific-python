from .client import Client


class GroupUsers:
    def __init__(self, client):
        self.__client = client

    def create_group_users(self, values: dict):
        return self.__client.request('post', '/group_users', data=values)
